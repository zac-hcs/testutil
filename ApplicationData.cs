﻿namespace Hcs.Util
{
   using System;

   /// <summary>
   /// Store data for the running application (global access), across threads
   /// once the application is stopped the cache data will be lost.
   /// </summary>
   public interface IApplicationData
   {
      /// <summary>
      /// Remove the item from the persistent storage.
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      void Remove(string key);

      /// <summary>
      /// Accessor for getting and setting items within the persistent storage.
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      /// <returns></returns>
      object this[string key] { get; set; }

      /// <summary>
      /// Insert the item into the persistent storage.
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      /// <param name="value">The item to store.</param>
      /// <param name="slidingExpiration">How long before removing the item from persistent storage after a last access.</param>
      void Insert(string key, object value, TimeSpan slidingExpiration);

      /// <summary>
      /// Insert the item into the persistent storage.
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      /// <param name="value">The item to store.</param>
      /// <param name="absoluteExpiration">The time when the item will be removed from the persistent storage.</param>
      void Insert(string key, object value, DateTime absoluteExpiration);


      /// <summary>
      /// Get the item from persistent storage, inserting if not found
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      /// <param name="value">Delegate to return the item for inserting</param>
      /// <param name="absoluteExpiration">The time when the item will be removed from the persistent storage.</param>
      /// <returns>The item from the persistent storage.</returns>
      object GetOrInsert(string key, Func<object> value, DateTime absoluteExpiration);

      /// <summary>
      /// Get the item from persistent storage, inserting if not found
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      /// <param name="value">Delegate to return the item for inserting</param>
      /// <param name="slidingExpiration">How long before removing the item from persistent storage after a last access.</param>
      /// <returns></returns>
      object GetOrInsert(string key, Func<object> value, TimeSpan slidingExpiration);
   }

}
