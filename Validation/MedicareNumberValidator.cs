﻿namespace Hcs.Util.Validation
{
   using System;

   public class MedicareNumberValidator 
   {
      public bool Validate(string valueToValidate, out string message)
      {
         message = "Medicare number is not valid";
         //check for null
         if (String.IsNullOrEmpty(valueToValidate))
            return false;

         //medicare rules...
         char[] digits = valueToValidate.ToCharArray();
         int sum = 0;
         var factors = new[] { 1, 3, 7, 9, 1, 3, 7, 9 };

         //card number must be atleast 9 chars to calculate check sum
         if (digits.Length < 11)
         {
            message = "Medicare number is not valid. Must be at least 11 digits";
            return false;
         }

         //if (digits.Length > 11)
         //{
         //   message = "Medicare number is not valid. Must be 11 digits or less";
         //   return false;
         //}

         long parsedVal;


         //check if the string is actually a number
         if (!Int64.TryParse(valueToValidate, out parsedVal))
         {
            message = "Medicare number is not valid. Must be a number";
            return false;
         }

         for (int i = 0; i <= 8; i++)
         {
            char digit = digits[i];
            var currentVal = int.Parse(digit.ToString());

            //the first digit must be between 2 and 6
            if (i == 0 && (currentVal < 2 || currentVal > 6))
            {
               message = "Medicare number is not valid. The first digit must be between 2 and 6";
               return false;
            }

            //9th digit is the check digit and must be equal the remainder of the sum of the 0-8 'multiplied' digits
            if (i == 8)
            {
               if ((sum % 10) != currentVal)
                  return false;
            }
            else //add the digit to the check sum - multiply by the factor.
            {
               sum += (currentVal * factors[i]);
            }
         }

         message = null;
         return true;
      }
   }
}

