namespace Hcs.Util
{
   using System;
   using System.Web;
   using System.Web.Caching;

   /// <summary>
   /// Store data for the running application (global access), across threads
   /// once the application is stopped the cache data will be lost.
   /// </summary>
   public static class ApplicationDataCache
   {
      private static readonly IApplicationData _data;

      /// <summary>
      /// Access to the actual data
      /// </summary>
      public static IApplicationData Data
      {
         get
         {
            return _data;
         }
      }

      static ApplicationDataCache()
      {
         if (RunningInWeb) _data = new WebApplicationData();
         else throw new NotImplementedException("ApplicationPersistentCache no implemented outside webcontext");
         //use:ObjectCache Class for application caching
         
      }

      private static bool RunningInWeb
      {
         get
         {
            return (HttpContext.Current != null);
         }
      }
   }

   /// <summary>
   /// Store data across requests for the running web application
   /// </summary>
   public class WebApplicationData : IApplicationData
   {
      //NOTE: there are still issues when IIS has multiple worker processors
      //cache need to be configured as SQL or service
      public Cache CacheProvider = HttpRuntime.Cache;

      public object this[string key]
      {
         get
         {
            return CacheProvider[key];
         }
         set
         {
            CacheProvider[key] = value;
         }
      }

      public void Remove(string key)
      {
         CacheProvider.Remove(key);
      }

      public void Insert(string key, object value, TimeSpan slidingExpiration)
      {
         CacheProvider.Insert(key, value, null, Cache.NoAbsoluteExpiration, slidingExpiration);
      }

      public void Insert(string key, object value, DateTime absoluteExpiration)
      {
         CacheProvider.Insert(key, value, null, absoluteExpiration, Cache.NoSlidingExpiration);
      }

      public object GetOrInsert(string key, Func<object> value, DateTime absoluteExpiration)
      {
         var t = this[key];

         if (t == null)
         {
            t = value();
            Insert(key, t, absoluteExpiration);
         }
         return t;
      }

      public object GetOrInsert(string key, Func<object> value, TimeSpan slidingExpiration)
      {
         var t = this[key];

         if (t == null)
         {
            t = value();
            Insert(key, t, slidingExpiration);
         }
         return t;
      }
   }
}