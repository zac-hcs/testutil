﻿namespace Hcs.Util.Conversion
{
   using System;
   using System.Globalization;
   using Ranges;

   public static class WebTypeConverter
   {
      public static T ConvertToTypedValue<T>(object dsValue)
      {
         if (dsValue is T)
            return (T)dsValue;

         object answer = null;

         var isNullableValueType = typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>);

         if (!typeof(T).IsValueType || isNullableValueType)
         {
            if (null == dsValue)
               return (T)answer;

            var stringValue = dsValue as string;
            if (stringValue != null)
            {
               if (stringValue == string.Empty)
                  return (T)answer; // set to null initially
            }
         }

         var underlyingType = isNullableValueType ? typeof(T).GetGenericArguments()[0] : typeof(T);

         if (underlyingType == typeof(int))
         {
            // attempt conversion to int 32
            answer = Convert.ToInt32(dsValue, CultureInfo.CurrentCulture);
            return (T)answer;
         }
         if (underlyingType == typeof(double))
         {
            answer = Convert.ToDouble(dsValue, CultureInfo.CurrentCulture);
            return (T)answer;
         }
         if (underlyingType == typeof(DateTime))
         {
            answer = Convert.ToDateTime(dsValue, CultureInfo.CurrentCulture);
            return (T)answer;
         }
         if (underlyingType == typeof(string))
         {
            if (dsValue is double)
               answer = ((double)dsValue).ToString("0.##");
            else
               answer = dsValue.ToString();
            return (T)answer;
         }
         if (underlyingType == typeof(bool))
         {
            var stringValue = dsValue as string;
            answer = stringValue != null ? StringToBoolean(stringValue) : Convert.ToBoolean(dsValue);
         }
         if (underlyingType == typeof(LocalDateRange))
         {
            var stringValue = dsValue as string;
            answer = stringValue != null ? LocalDateRange.Parse(stringValue) : null;
         }

         // if we don't find what we want, then attempt a cast
         return (T)answer;
      }

      public static bool StringToBoolean(String booleanString)
      {
         if (string.IsNullOrEmpty(booleanString))
            return false;

         return string.Equals(booleanString, "yes", StringComparison.OrdinalIgnoreCase)
                || string.Equals(booleanString, "y", StringComparison.OrdinalIgnoreCase)
                || string.Equals(booleanString, "on", StringComparison.OrdinalIgnoreCase)
                || string.Equals(booleanString, "true", StringComparison.OrdinalIgnoreCase)
                || string.Equals(booleanString, "t", StringComparison.OrdinalIgnoreCase)
                || string.Equals(booleanString, "1", StringComparison.OrdinalIgnoreCase);
      }
   }
}
