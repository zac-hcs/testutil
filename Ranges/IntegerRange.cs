namespace Hcs.Applications.Common.Data.Util
{
   using System;

   public class IntegerRange
   {
      private const string DEFAULT_GREATERTHAN = ">";
      private const string DEFAULT_LESSTHAN = "<";
      
      private readonly int _start;
      private IntegerRangeMode _mode;
      
      public IntegerRange(int start, IntegerRangeMode mode)
      {
         _start = start;
         _mode = mode;
      }
      
      public int Start
      {
         get { return _start; }
      }

      public IntegerRangeMode Mode
      {
         get { return _mode; }
      }

      public override string ToString()
      {
         switch (_mode)
         {
            case IntegerRangeMode.Equal:
               return _start.ToString();
            case IntegerRangeMode.LessThan:
               return DEFAULT_LESSTHAN + _start;
            case IntegerRangeMode.GreaterThan:
               return DEFAULT_GREATERTHAN + _start;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      public static IntegerRange Parse(string integerRangeString)
      {
         if (integerRangeString == null)
            return null;

         var formattedRangeString = integerRangeString.Trim();

         if (string.IsNullOrEmpty(formattedRangeString))
            return null;

         foreach (IntegerRangeMode mode in (IntegerRangeMode[])Enum.GetValues(typeof(IntegerRangeMode)))
         {
            IntegerRange ret;
            if (TryParse(formattedRangeString, mode, out ret))
               return ret;
         }

         //Throw?
         return null;
      }

      public static bool TryParse(string inputString, IntegerRangeMode mode, out IntegerRange range)
      {
         switch (mode)
         {
            case IntegerRangeMode.Equal:
               return TryParseEqual(inputString, out range);
            case IntegerRangeMode.LessThan:
               return TryParseLessThan(inputString, out range);
            case IntegerRangeMode.GreaterThan:
               return TryParseGreaterThan(inputString, out range);
            default:
               throw new ArgumentOutOfRangeException("mode");
         }
      }

      private static bool TryParseEqual(string inputString, out IntegerRange range)
      {
         int startValue;

         if (Int32.TryParse(inputString, out startValue))
         {
            range = new IntegerRange(startValue, IntegerRangeMode.Equal);
            return true;
         }

         range = null;
         return false;
      }

      private static bool TryParseLessThan(string inputString, out IntegerRange range)
      {
         int startvalue;

         if (inputString.StartsWith(DEFAULT_LESSTHAN) && Int32.TryParse(inputString.Substring(DEFAULT_LESSTHAN.Length), out startvalue))
         {
            range = new IntegerRange(startvalue, IntegerRangeMode.LessThan);
            return true;
         }

         range = null;
         return false;
      }

      private static bool TryParseGreaterThan(string inputString, out IntegerRange range)
      {
         int startvalue;

         if (inputString.StartsWith(DEFAULT_GREATERTHAN) && Int32.TryParse(inputString.Substring(DEFAULT_GREATERTHAN.Length), out startvalue))
         {
            range = new IntegerRange(startvalue, IntegerRangeMode.GreaterThan);
            return true;
         }

         range = null;
         return false;
      }
   }

   public enum IntegerRangeMode
   {
      Equal,
      LessThan, 
      GreaterThan
   }
}