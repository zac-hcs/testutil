using NodaTime;
using NodaTime.Text;

namespace Hcs.Util.Ranges
{
   using System;
   using System.Globalization;

   public class LocalDateRange : IEquatable<LocalDateRange>
   {
      private const string DefaultDateSeparator = "..";

      public LocalDate StartDate { get; private set; }
      public LocalDate EndDate { get; private set; }

      public DateTime StartDateAsDateTime
      {
         get { return new DateTime(StartDate.Year, StartDate.Month, StartDate.Day); }
      }

      public DateTime EndDateAsDateTime
      {
         get { return new DateTime(EndDate.Year, EndDate.Month, EndDate.Day); }
      }

      public DateRangeGranularity Granularity { get; private set; }

      public LocalDateRange(LocalDate startDate, LocalDate endDate, DateRangeGranularity granularity)
      {
         StartDate = ConvertStartDateGranularity(startDate, granularity);
         EndDate = ConvertEndDateGranularity(endDate, granularity);
         Granularity = granularity;
      }

      public LocalDateRange(LocalDate startAndEndDate)
         : this(startAndEndDate, startAndEndDate, DateRangeGranularity.Date)
      {
         
      }

      private static LocalDate ConvertStartDateGranularity(LocalDate date, DateRangeGranularity granularity)
      {
         switch (granularity)
         {
            case DateRangeGranularity.Date:
               return date;
            case DateRangeGranularity.Month:
               return date.PlusDays(-(date.Day-1)); // Start of month
            case DateRangeGranularity.Year:
               return date.PlusDays(-(date.DayOfYear-1)); // Start of year
            default:
               throw new ArgumentException("Unknown granularity", "granularity");
         }
      }

      private static LocalDate ConvertEndDateGranularity(LocalDate date, DateRangeGranularity granularity)
      {
         switch (granularity)
         {
            case DateRangeGranularity.Date:
               return date;
            case DateRangeGranularity.Month:
               return date.PlusDays(-date.Day).PlusMonths(1); // End of month
            case DateRangeGranularity.Year:
               return date.PlusDays(-date.DayOfYear).PlusYears(1); // End of year
            default:
               throw new ArgumentException("Unknown granularity", "granularity");
         }
      }

      public override bool Equals(object obj)
      {
         if (ReferenceEquals(null, obj)) return false;
         if (ReferenceEquals(this, obj)) return true;
         if (obj.GetType() != typeof(LocalDateRange)) return false;
         return Equals((LocalDateRange)obj);
      }

      public override string ToString()
      {
         if (StartDate == EndDate)
            return StartDate.AtMidnight().ToDateTimeUnspecified().ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);

         return string.Format("{0}" + DefaultDateSeparator + "{1}",
                              StartDate.AtMidnight().ToDateTimeUnspecified().ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern),
                              EndDate.AtMidnight().ToDateTimeUnspecified().ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern));
      }

      /// <summary>
      /// Returns a parsed date range using the default date separator (..).  
      /// </summary>
      /// <param name="dateTimeString">The date time string to convert to a date range.</param>
      /// <returns>The date range to attempt to convert to a date range.</returns>
      public static LocalDateRange Parse(string dateTimeString)
      {
         return Parse(dateTimeString, DefaultDateSeparator);
      }

      /// <summary>
      /// Returns a parsed date range using a provided <paramref name="dateSeparator"/>.
      /// </summary>
      /// <param name="dateTimeString">The date time string to be parsed into a date range.</param>
      /// <param name="dateSeparator">The separator to use when parsing the date range.  This is the divider between the two dates.</param>
      /// <returns>The date range to be returned.</returns>
      public static LocalDateRange Parse(string dateTimeString, string dateSeparator)
      {
         if (string.IsNullOrEmpty(dateTimeString))
            return null;

         string[] returnedParts = dateTimeString.Split(new[] { dateSeparator }, StringSplitOptions.RemoveEmptyEntries);

         if (returnedParts.Length == 0 || string.IsNullOrEmpty(returnedParts[0]))
            return null;

         try
         {
            var firstPart = ParseDatePart(returnedParts[0], false);
            var secondPart = returnedParts.Length > 1 ? ParseDatePart(returnedParts[1], true) : ParseDatePart(returnedParts[0], true);

            // Each part may be a different granularity, so we use the most granular for the whole range
            if (firstPart.Granularity == secondPart.Granularity || firstPart.Granularity > secondPart.Granularity)
               return new LocalDateRange(firstPart.ParsedDate, secondPart.ParsedDate, firstPart.Granularity);

            return new LocalDateRange(firstPart.ParsedDate, secondPart.ParsedDate, secondPart.Granularity);
         }
         catch (FormatException)
         {
            return null;
         }
      }


      private static ParsedLocalDate ParseDatePart(string dateTimeString, bool endOfPeriodBias)
      {
         var part = new ParsedLocalDate();

         var dateParts = dateTimeString.Split(new[] {'/', '-', ' '}, StringSplitOptions.RemoveEmptyEntries);
         
         if (dateParts.Length == 0)
            throw new FormatException("The date range entered is not valid.  It contains only /'s -'s or spaces");

         if (dateParts.Length > 2)
         {
            var datePattern = LocalDatePattern.Create("d", CultureInfo.CurrentCulture);
            var parseResult = datePattern.Parse(dateTimeString);

            if (parseResult.Success)
            {
               part.ParsedDate = parseResult.Value;
               part.Granularity = DateRangeGranularity.Date;
               return part;
            }
            throw new FormatException("Encountered an invalid character in the date range string.");
         }

         if (dateParts.Length == 2)
         {
            // this is a month and year combination
            // or date and month combination
            bool? isMonthYear = null;
            int firstPartNumber;
            int secondPartNumber;
            if (int.TryParse(dateParts[0], out firstPartNumber))
            {
               // but still has to fit between 1 and 31
               if (firstPartNumber < 1 || firstPartNumber > 31)
                  throw new FormatException("Encountered an invalid character in the date range string.");

               // this is not yet decided whether this should be month/year or day/month
               // unless number is over 12
               if (firstPartNumber > 12)
                  isMonthYear = false;
            }
            else
            {
               if (dateParts[0].Length < 3)
                  throw new FormatException("Encountered an invalid character in the date range string.");

               var monthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
               for (var i = 0; i < monthNames.Length; i++)
               {
                  if (monthNames[i].Length > 0 && string.Equals(dateParts[0].Substring(0, 3), monthNames[i].Substring(0, 3), StringComparison.OrdinalIgnoreCase))
                  {
                     isMonthYear = true;
                     firstPartNumber = i + 1;
                     break;
                  }
               }
               if (null == isMonthYear)
                  throw new FormatException("Encountered an invalid character in the date range string.");
            }

            // Now deal with the second number
            if (int.TryParse(dateParts[1], out secondPartNumber))
            {
               if (secondPartNumber < 0)
                  throw new FormatException("Encountered an invalid character in the date range string.");

               // if still undecided about month year, then this decision can be made now
               // undecided means it can still go either way
               if (isMonthYear == null)
                  isMonthYear = !(secondPartNumber > 0 || secondPartNumber < 13);
            }
            else if (null != isMonthYear && isMonthYear.Value)
            {
               // already identified as month year so this is an error
               throw new FormatException("Encountered an invalid character in the date range string.");
            }
            else
            {
               if (dateParts[1].Length < 3)
                  throw new FormatException("Encountered an invalid character in the date range string.");

               // this must be date/month
               var monthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
               for (var i = 0; i < monthNames.Length; i++)
               {
                  if (monthNames[i].Length > 0 && string.Equals(dateParts[1].Substring(0, 3), monthNames[i].Substring(0, 3), StringComparison.OrdinalIgnoreCase))
                  {
                     isMonthYear = false;
                     secondPartNumber = i + 1; // add 1 to convert from 0-based array index
                     break;
                  }
               }
               if (null == isMonthYear)
                  throw new FormatException("Encountered an invalid character in the date range string.");
            }

            if (isMonthYear.Value)
            {
               if (dateParts[1].Length < 4)
                  part.ParsedDate = new LocalDate(DateTime.Now.Year - (DateTime.Now.Year % (dateParts[1].Length == 3 ? 1000 : 100)) + secondPartNumber, firstPartNumber, 1);
               else
                  part.ParsedDate = new LocalDate(secondPartNumber, firstPartNumber, 1);
               if (endOfPeriodBias)
                  part.ParsedDate = part.ParsedDate.PlusMonths(1).PlusDays(-1);
               part.Granularity = DateRangeGranularity.Month;
            }
            else
            {
               if (secondPartNumber > 12)
                  throw new FormatException("Encountered an invalid combination in the date range string");
               part.ParsedDate = new LocalDate(DateTime.Now.Year, secondPartNumber, firstPartNumber);
               part.Granularity = DateRangeGranularity.Date;
            }
         }
         else // in this case, this is a single year (2004) or month year together (032006)
         {
            int parsedInt;
            if (Int32.TryParse(dateParts[0], out parsedInt))
            {
               if (parsedInt <= 0)
                  throw new FormatException("Encountered an invalid part of date range");
               if (parsedInt > 129999)
                  throw new FormatException("Encountered an invalid part of date range (upper limit is december 9999)");

               // if the first date part is 1 character, then this is a year
               // user input only one digit

               if (dateParts[0].Length == 1)
               {
                  var lastTwoDigits = DateTime.Now.Year % 100;
                  var firstTwoDigits = DateTime.Now.Year / 100;

                  if (lastTwoDigits <= 50)
                  {
                     part.ParsedDate = new LocalDate((firstTwoDigits * 100) + int.Parse(dateParts[0]),1,1);
                  }
                  else
                  {
                     part.ParsedDate = new LocalDate(((firstTwoDigits+1) * 100) + int.Parse(dateParts[0]),1,1);
                  }
               }
               else
               {
                  var monthPart = int.Parse(dateParts[0].Substring(0, 2));
                  if (dateParts[0].Length < 4 || monthPart > 12 || monthPart < 1)
                  {
                     // This is a straight year, which cannot be greater than 9999
                     if (dateParts[0].Length > 4)
                        throw new FormatException("Encountered an invalid part of date range.");

                     if (dateParts[0].Length < 4)
                        part.ParsedDate = new LocalDate(DateTime.Now.Year - (DateTime.Now.Year % (dateParts[0].Length == 3 ? 1000 : 100)) + parsedInt, 1, 1);
                     else
                        part.ParsedDate = new LocalDate(parsedInt, 1, 1);

                     if (endOfPeriodBias)
                        part.ParsedDate = part.ParsedDate.PlusYears(1).PlusDays(-1);

                     part.Granularity = DateRangeGranularity.Year;
                  }
                  else
                  {
                     if (dateParts[0].Length < 6)
                        part.ParsedDate = new LocalDate(DateTime.Now.Year - (DateTime.Now.Year % (dateParts[0].Length == 5 ? 1000 : 100)) + int.Parse(dateParts[0].Substring((2))), monthPart, 1);
                     else
                        part.ParsedDate = new LocalDate(int.Parse(dateParts[0].Substring(2)), monthPart, 1);
                     if (endOfPeriodBias)
                        part.ParsedDate = part.ParsedDate.PlusMonths(1).PlusDays(-1);
                     part.Granularity = DateRangeGranularity.Month;
                  }
               }               
            }
            else
            {
               if (dateParts[0].Length < 3)
                  throw new FormatException("Encountered an invalid character in the date range string.");

               // if this is not an integer, then it must be a month name.
               var monthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
               var foundMonthMatch = false;
               for (var i = 0; i < monthNames.Length; i++)
               {
                  if (monthNames[i].Length > 0 && string.Equals(dateParts[0].Substring(0, 3), monthNames[i].Substring(0, 3), StringComparison.OrdinalIgnoreCase))
                  {
                     part.ParsedDate = new LocalDate(DateTime.Now.Year, i + 1, 1); // add 1 to month number to convert from 0-based array index
                     if (endOfPeriodBias)
                        part.ParsedDate = part.ParsedDate.PlusMonths(1).PlusDays(-1);
                     part.Granularity = DateRangeGranularity.Month;
                     foundMonthMatch = true;
                     break;
                  }
               }
               if (!foundMonthMatch)
                  throw new FormatException("Encountered an invalid character in the date range string.");
            }
         }
         return part;
      }

      private struct ParsedLocalDate
      {
         internal LocalDate ParsedDate;
         internal DateRangeGranularity Granularity;
      }

      public bool Equals(LocalDateRange other)
      {
         if (ReferenceEquals(null, other)) return false;
         if (ReferenceEquals(this, other)) return true;
         return other.StartDate.Equals(StartDate) && other.EndDate.Equals(EndDate) && Equals(other.Granularity, Granularity);
      }

      public override int GetHashCode()
      {
         unchecked
         {
            int result = StartDate.GetHashCode();
            result = (result*397) ^ EndDate.GetHashCode();
            result = (result*397) ^ Granularity.GetHashCode();
            return result;
         }
      }
   }

}