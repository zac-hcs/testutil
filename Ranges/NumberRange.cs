namespace Hcs.Util.Ranges
{
   public class NumberRange
   {
      private readonly double _lowestNumber;
      private readonly double _highestNumber;

      public NumberRange(double lowestNumber, double highestNumber)
      {
         if (lowestNumber > highestNumber)
         {
            _lowestNumber = highestNumber;
            _highestNumber = lowestNumber;
         }
         else
         {
            _lowestNumber = lowestNumber;
            _highestNumber = highestNumber;
         }
      }

      public NumberRange(double fixedNumber)
      {
         _lowestNumber = fixedNumber;
         _highestNumber = fixedNumber;
      }

      public double LowestNumber
      {
         get { return _lowestNumber; }
      }

      public double HighestNumber
      {
         get { return _highestNumber; }
      }

      public double? FixedNumber
      {
         get
         {
            if (!IsNumberRange)
               return _lowestNumber;
            return null;
         }
      }

      public bool IsNumberRange
      {
         get { return LowestNumber != HighestNumber; }
      }

      private static NumberRange CreateFixedNumber(string stringValue)
      {
         double numberValue;
         if (double.TryParse(stringValue, out numberValue))
            return new NumberRange(numberValue);
         return null;
      }

      public override string ToString()
      {
         return ToString("0.##");
      }

      public string ToString(string doubleFormat)
      {
         if (IsNumberRange)
            return string.Concat(LowestNumber.ToString(doubleFormat), "-", HighestNumber.ToString(doubleFormat));
         return LowestNumber.ToString(doubleFormat);
      }

      public static bool TryParse(string stringValue, out NumberRange range)
      {
         if (stringValue.Contains("-") || stringValue.Contains(","))
         {
            var values = stringValue.Split(new[] { '-', ',' });
            if (values.Length == 1)
            {
               var fixedNumber = CreateFixedNumber(values[0]);
               if (fixedNumber != null)
               {
                  range = fixedNumber;
                  return true;
               }
            }

            double lowerValue;
            double upperValue;
            if (double.TryParse(values[0], out lowerValue) && double.TryParse(values[1], out upperValue))
            {
               range = new NumberRange(lowerValue, upperValue);
               return true;
            }
         }
         var fixedNumberRange = CreateFixedNumber(stringValue);
         if (fixedNumberRange != null)
         {
            range = fixedNumberRange;
            return true;
         }

         range = default(NumberRange);
         return true;
      }

      public NumberRange Clone()
      {
         return new NumberRange(_lowestNumber, _highestNumber);
      }
   }
}