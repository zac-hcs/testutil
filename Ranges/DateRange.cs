//-----------------------------------------------------------------------
// <copyright file="DateRange" company="Healthcare Software">
//     Copyright (c) Healthcare Software. All rights reserved.
// </copyright>
// <author>Christian De Kievit</author>
//-----------------------------------------------------------------------


namespace Hcs.Util.Ranges
{
   using NodaTime;
   using System;

   /// <summary>
   /// The granularity of a date range.
   /// </summary>
   /// <remarks>
   /// Must be ordered from least granular (E.g. year) to most granular (E.g. day) as it is used in comparisons.
   /// </remarks>
   public enum DateRangeGranularity
   {
       Year
      ,Month
      ,Date
   }

   public static class LocalDateRangeExtensions
   {
      public static bool IsInDateRange(this LocalDateTime dateTime, LocalDateRange range)
      {
         var date = dateTime.Date;
         if (range.Granularity == DateRangeGranularity.Date)
            return date >= range.StartDate && date <= range.EndDate;
         if (range.Granularity == DateRangeGranularity.Month)
            return date >= range.StartDate && date < range.EndDate.PlusMonths(1);
         if (range.Granularity == DateRangeGranularity.Year)
            return date >= range.StartDate && date < range.EndDate.PlusYears(1);
         throw new ArgumentOutOfRangeException("range", "Range Granularity is in correct");
      }

      public static bool IsInDateRange(this LocalDate date, LocalDateRange range)
      {
         if (range.Granularity == DateRangeGranularity.Date)
            return date >= range.StartDate && date <= range.EndDate;
         if (range.Granularity == DateRangeGranularity.Month)
            return date >= range.StartDate && date < range.EndDate.PlusMonths(1);
         if (range.Granularity == DateRangeGranularity.Year)
            return date >= range.StartDate && date < range.EndDate.PlusYears(1);
         throw new ArgumentOutOfRangeException("range", "Range Granularity is in correct");
      }

      public static bool OverlapsDateRange(this LocalDateRange range, LocalDateTime? startDate, LocalDateTime? endDate)
      {
         return OverlapsDateRange(range, startDate?.Date, endDate?.Date);
      }

      public static bool OverlapsDateRange(this LocalDateRange range, LocalDate? startDate, LocalDate? endDate)
      {
         if (!startDate.HasValue && !endDate.HasValue)
            return true;

         // If there's only an end date, then just make sure the end date is greater than the start date of the range
         if (!startDate.HasValue)
            return range.StartDate <= endDate.Value;

         // If there's only a start date, then just make sure the start date is less than the end date + 1 time period
         if (!endDate.HasValue)
         {
            if (range.Granularity == DateRangeGranularity.Date)
               return startDate.Value <= range.EndDate;
            if (range.Granularity == DateRangeGranularity.Month)
               return startDate.Value < range.EndDate.PlusMonths(1);
            if (range.Granularity == DateRangeGranularity.Year)
               return startDate.Value < range.EndDate.PlusYears(1);
            throw new ArgumentOutOfRangeException("range", "Range Granularity is invalid");
         }

         // If both, use a standard overlap check (x.start < (y.end + 1 time period) && x.end >= y.start
         if (range.Granularity == DateRangeGranularity.Date)
            return startDate.Value < range.EndDate.PlusDays(1) && endDate.Value >= range.StartDate;
         if (range.Granularity == DateRangeGranularity.Month)
            return startDate.Value < range.EndDate.PlusMonths(1) && endDate.Value >= range.StartDate;
         if (range.Granularity == DateRangeGranularity.Year)
            return startDate.Value < range.EndDate.PlusYears(1) && endDate.Value >= range.StartDate;
         throw new ArgumentOutOfRangeException("range", "Range Granularity is invalid");
      }
   }
}