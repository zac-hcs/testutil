﻿namespace Hcs.Util
{
   using System.Collections.Generic;
   using System.Dynamic;

   public static class DynamicExtensions
   {
      /// <summary>
      /// https://stackoverflow.com/questions/42836936/convert-class-to-dynamic-and-add-properties
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="obj"></param>
      /// <returns></returns>
      public static dynamic ToDynamic<T>(this T obj)
      {
         IDictionary<string, object> expando = new ExpandoObject();

         foreach (var propertyInfo in typeof(T).GetProperties())
         {
            var currentValue = propertyInfo.GetValue(obj);
            expando.Add(propertyInfo.Name, currentValue);
         }
         return expando as ExpandoObject;
      }
   }
}