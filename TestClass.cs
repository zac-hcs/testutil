﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Hcs.Util
{
   public class TestClass
   {
      public TestClass() { }

      public string TestMethod()
      {
         return "THIS IS A TEST METHOD";
      }

      public string GetVersion()
      {
         return GetType().Assembly.GetName().Version.ToString();
      }

      public string NewMethod()
      {
         return "new method";
      }
   }
}
