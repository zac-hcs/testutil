﻿namespace Hcs.Web.Context
{
   using System;

   public interface IControlContext
   {
      void EnteringContext(Type typeName, string contextIdentifier, string contextDescription);
      void LeavingContext(Type typeName, string contextIdentifier);

      string RenderCurrentContext();
   }
}
