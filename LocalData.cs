﻿namespace Hcs.Util
{
   public interface ILocalData
   {
      /// <summary>
      /// Use to remove all data from the local storage
      /// </summary>
      void Clear();
      
      /// <summary>
      /// Accessor for getting and setting items within the local storage hashtable.
      /// </summary>
      /// <param name="key">The object to index the store by.</param>
      /// <returns></returns>
      object this[object key] { get; set; }
   }
}
