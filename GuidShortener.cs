﻿using System;

namespace Hcs.Util
{
   /// <summary>
   /// Reversibly shortens (ie: encodes) a GUID to 22 character CASE SENSITIVE string.
   /// This can be used in URLs as long as the case is preserved.
   /// See https://madskristensen.net/blog/A-shorter-and-URL-friendly-GUID
   /// https://stackoverflow.com/questions/9278909/net-short-unique-identifier
   /// https://www.singular.co.nz/2007/12/shortguid-a-shorter-and-url-friendly-guid-in-c-sharp/
   /// </summary>
   public static class GuidShortener
   {
      public static string Encode(string guidText)
      {
         var guid = new Guid(guidText);
         return Encode(guid);
      }

      public static string Encode(Guid guid)
      {
         var enc = Convert.ToBase64String(guid.ToByteArray());
         enc = enc.Replace("/", "_");
         enc = enc.Replace("+", "-");
         return enc.Substring(0, 22);
      }

      public static Guid Decode(string encoded)
      {
         encoded = encoded.Replace("_", "/");
         encoded = encoded.Replace("-", "+");
         var buffer = Convert.FromBase64String(encoded + "==");
         return new Guid(buffer);
      }
   }
}