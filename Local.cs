namespace Hcs.Util
{
   using System;
   using System.Collections;
   using System.Web;
   using System.Runtime.Remoting.Messaging;

   /// <summary>
   /// Provides thread isolated access to local storage working within a Http Context 
   /// or in a local thread
   /// </summary>
   public static class Local
   {
      private static readonly LocalData _data = new LocalData();

      /// <summary>
      /// Access to the actual data
      /// </summary>
      public static ILocalData Data
      {
         get
         {
            return _data;
         }
      }

      public static T GetOrStoreCachedData<T>(object key, Func<T> func)
      {
         if (!_data.HasKey(key))
            _data[key] = func();
         return (T)_data[key];
      }

      private class LocalData : ILocalData
      {
         private const string LocalDataHashtableKey = "hcs.context.cache";

         public void Clear()
         {
            LocalHashtable.Clear();
         }

         public bool HasKey(object key)
         {
            return LocalHashtable.ContainsKey(key);
         }

         public object this[object key]
         {
            get
            {
               return LocalHashtable[key];
            }
            set
            {
               LocalHashtable[key] = value;
            }
         }

         private static Hashtable LocalHashtable
         {
            get
            {
               if (!RunningInWeb)
               {
                  if (CallContext.GetData(LocalDataHashtableKey) == null)
                  {
                     CallContext.SetData(LocalDataHashtableKey, new Hashtable());
                  }
                  return (Hashtable)CallContext.GetData(LocalDataHashtableKey);
               }

               var hashtable = HttpContext.Current.Items[LocalDataHashtableKey] as Hashtable;
               if (hashtable == null)
               {
                  hashtable = new Hashtable();
                  HttpContext.Current.Items[LocalDataHashtableKey] = hashtable;
               }

               return hashtable;
            }
         }

         private static bool RunningInWeb
         {
            get
            {
               return (HttpContext.Current != null);
            }
         }
      }
   }
}