﻿namespace Hcs.Util
{
   using System;

   public static class GuidComb
   {
      /// <summary>
      /// Generate a new <see cref="Guid"/> using the comb algorithm.
      /// Copied from the NHibernate Guid.Comb implementation for use outside of the environment
      /// NOTE: Ordering will only work correctly in SQL Server, so best not to use if ordering is required
      /// outside of the SQL Server environment
      /// For more information on how Guids are compared in SQL Server, visit https://blogs.msdn.microsoft.com/sqlprogrammability/2006/11/06/how-are-guids-compared-in-sql-server-2005/
      /// </summary>
      public static Guid GenerateSqlServerSequentialGuid()
      {
         var guidArray = Guid.NewGuid().ToByteArray();
         var baseDate = new DateTime(1900, 1, 1);
         var now = DateTime.UtcNow;
         
         // Get the days and milliseconds which will be used to build the byte string
         var days = new TimeSpan(now.Ticks - baseDate.Ticks);
         var msecs = now.TimeOfDay;

         // Convert to a byte array
         // Note that SQL Server is accurate to 1/300th of a millisecond so we divide by 3.333333
         var daysArray = BitConverter.GetBytes(days.Days);
         var msecsArray = BitConverter.GetBytes((long)(msecs.TotalMilliseconds / 3.333333));

         // Reverse the bytes to match SQL Servers ordering
         Array.Reverse(daysArray);
         Array.Reverse(msecsArray);

         // Copy the bytes into the guid
         Array.Copy(daysArray, daysArray.Length - 2, guidArray, guidArray.Length - 6, 2);
         Array.Copy(msecsArray, msecsArray.Length - 4, guidArray, guidArray.Length - 4, 4);

         return new Guid(guidArray);
      }
   }
}

