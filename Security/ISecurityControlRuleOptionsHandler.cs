﻿namespace Hcs.Util.Security
{
   public interface ISecurityControlRuleOptionsHandler
   {
      bool UserIsAllowed(string username, string operationName);
      bool CurrentUserIsAllowed(string operationName);
   }

   /// <summary>
   /// All options used in SecurityControlRules.txt must be listed here - you have been warned!
   /// </summary>
   public static class ControlRuleOptionsHandlers
   {
      public const string BlockedByMimsUpdate = "BlockedByMimsUpdate";
      public const string CheckTaskCreationElevation = "TaskCreationChecker";
      public const string CheckTaskEditingElevation = "TaskEditingChecker";
   }
}
