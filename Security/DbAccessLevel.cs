namespace Hcs.Util.Security
{
   public enum DbAccessLevel
   {
      NoAccess = 0,
      ReadOnly = 1,
      Modify = 2,
      New = 3,
      Full = 4
   }
}