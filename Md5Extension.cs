﻿using System;
using System.Text;

namespace Hcs.Util
{
   using System.Security.Cryptography;

   public static class Md5Extension
   {
      public static byte[] GetMd5(this string s)
      {
         var bytes=Encoding.Unicode.GetBytes(s);
         return bytes.GetMd5();
      }

      public static byte[] GetMd5(this bool value)
      {
         return BitConverter.GetBytes(value).GetMd5();
      }

      public static byte[] GetMd5(this int value)
      {
         return BitConverter.GetBytes(value).GetMd5();
      }

      public static byte[] GetMd5(this byte[] value)
      {
         return MD5.Create().ComputeHash(value);
      }
   }
}
