using System;
namespace Hcs.Util
{
   public static class CloneHelper
   {
      public static T TypedClone<T>(this T source) where T : class, ICloneable
      {
         if (source == null)
            return default(T);

         return source.Clone() as T;
      }
   }
}