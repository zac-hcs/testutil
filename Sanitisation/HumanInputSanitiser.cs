//-----------------------------------------------------------------------
// <copyright file="InputSanitiser" company="Healthcare Software">
//     Copyright (c) Healthcare Software. All rights reserved.
// </copyright>
// <author>Christian De Kievit</author>
//-----------------------------------------------------------------------
namespace Hcs.Util.Sanitisation
{
   using System.Text;
   using System;
   using System.Text.RegularExpressions;
   using Encoder = Microsoft.Security.Application.Encoder;

   public static class HumanInputSanitiser
   {
      private static readonly Regex _tags = new Regex("<[^>\n]+>", RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.Compiled);
      private static readonly Regex _blacklist = new Regex("^<[^a-zA-Z]*(script|noscript|param|html|body|applet|embed|object|meta|link|style|frameset|frame|iframe|img)[^a-zA-Z]", RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace | RegexOptions.ExplicitCapture | RegexOptions.Compiled | RegexOptions.IgnoreCase);
      private static readonly Regex _badAttributeMatch = new Regex(@"(style|href|link|on[a-zA-Z]+)\s*=\s*(""[^""]+""|'[^']+'|[^\s""'/>]+)", RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase | RegexOptions.Compiled);

      public static string FixUrlInput(string input)
      {
         return Encoder.UrlEncode(input);
      }

      public static string FixSqlInput(string input)
      {
         if (string.IsNullOrEmpty(input))
            return input;

         return input.Replace("'", "''");
      }

      public static string FixForHtml(this string input)
      {
         return Encoder.HtmlEncode(input);
      }

      public static string FixForHtmlAttribute(string input)
      {
         return Encoder.HtmlAttributeEncode(input);
      }

      public static string FixForJavascript(string input)
      {
         return EncodeJs(input);
      }

      private static string EncodeJs(string strInput)
      {
         if (string.IsNullOrEmpty(strInput))
         {
            return string.Empty;
         }

         var builder = new StringBuilder(strInput.Length * 2);
         foreach (var ch in strInput)
         {
            if ((((ch > '`') && (ch < '{')) || ((ch > '@') && (ch < '['))) || (((ch == ' ') || ((ch > '/') && (ch < ':'))) || (((ch == '.') || (ch == ',')) || ((ch == '-') || (ch == '_')))))
            {
               builder.Append(ch);
            }
            else if (ch > '\x007f')
            {
               builder.Append(@"\u" + TwoByteHex(ch));
            }
            else
            {
               builder.Append(@"\x" + SingleByteHex(ch));
            }
         }

         return builder.ToString();
      }

      private static string TwoByteHex(char c)
      {
         uint num = c;
         return num.ToString("x").PadLeft(4, '0');
      }

      public static string FixForSqlDate(string input)
      {
         DateTime actualDate;
         if (DateTime.TryParse(input, out actualDate))
            return actualDate.ToString("dd/MM/yyyy");
         
         return string.Empty;
      }

      private static string SingleByteHex(char c)
      {
         uint num = c;
         return num.ToString("x").PadLeft(2, '0');
      }

      /// <summary>
      /// Sanitise any potentially dangerous tags from the provided raw HTML input using 
      /// a whitelist based approach, leaving the "safe" HTML tags
      /// </summary>
      public static string StripBadInputCharacters(string input)
      {
         if (string.IsNullOrEmpty(input))
            return input;

         var tags = _tags.Matches(input);

         // iterate through all HTML tags in the input
         for (var i = tags.Count - 1; i > -1; i--)
         {
            var tag = tags[i];
            var taglower = tag.Value.ToLower();

            if (_blacklist.IsMatch(taglower))
               input = input.Remove(tag.Index, tag.Length);
            else
            {
               // remove bad style attributes
               var tagValue = _badAttributeMatch.Replace(tag.Value, string.Empty);
               input = input.Remove(tag.Index, tag.Length).Insert(tag.Index, tagValue);
            }
         }
         return input;
      }

      /// <summary>
      /// Remove ALL tags from the input string
      /// </summary>
      public static string StripAllTags(string input)
      {
         if (string.IsNullOrEmpty(input))
            return input;

         var tags = _tags.Matches(input);

         // iterate through all tags in the input
         for (var i = tags.Count - 1; i > -1; i--)
         {
            var tag = tags[i];
            input = input.Remove(tag.Index, tag.Length);
         }
         return input;
      }


      public static string ConvertFromCamelOrPascalCase(string name)
      {
         if (string.IsNullOrEmpty(name))
            return name;

         var prevCharIsUpper = true;
         var first = true;
         var builder = new StringBuilder();
         foreach (var i in name)
         {
            if (first)
            {
               builder.Append(Char.ToUpper(i));
               first = false;
            }
            else if (Char.IsUpper(i))
            {
               if (!prevCharIsUpper)
               {
                  builder.Append(' ');
               }
               builder.Append(i);
               prevCharIsUpper = true;
            }
            else
            {
               builder.Append(i);
               prevCharIsUpper = false;
            }
         }
         return builder.ToString();
      }

      /// <summary>
      /// Strip Illegal XML Unicode Characters
      /// http://www.w3.org/TR/2004/REC-xml11-20040204/#NT-Char
      /// </summary>
      /// <param name="input"></param>
      /// <returns></returns>
      public static string StripIllegalXmlChars(string input)
      {
         var regex = new Regex(@"[\u0000-\u0008\u000B-\u000C\u000E-\u001F\u007F-\u0084\u0086-\u009F]", RegexOptions.IgnoreCase);
         if (regex.IsMatch(input))
         {
            input = regex.Replace(input, string.Empty);
         }

         return input;
      }


      /// <summary>
      /// Strip bad user inputs. Allow 'href' input for high risk meds catagory guidance
      /// </summary>
      public static string StripBadInputCharactersForHrmGuidance(string input)
      {
         var badAttributes = new Regex(@"(style|link|on[a-zA-Z]+)\s*=\s*(""[^""]+""|'[^']+'|[^\s""'/>]+)", RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase | RegexOptions.Compiled);

         if (string.IsNullOrEmpty(input))
            return input;

         var tags = _tags.Matches(input);

         // iterate through all HTML tags in the input
         for (var i = tags.Count - 1; i > -1; i--)
         {
            var tag = tags[i];
            var taglower = tag.Value.ToLower();

            if (_blacklist.IsMatch(taglower))
               input = input.Remove(tag.Index, tag.Length);
            else
            {
               // remove bad style attributes
               var tagValue = badAttributes.Replace(tag.Value, string.Empty);
               input = input.Remove(tag.Index, tag.Length).Insert(tag.Index, tagValue);
            }
         }

         return input;
      }
   }
}
