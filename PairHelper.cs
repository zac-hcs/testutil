﻿namespace Hcs.Util
{
   using Castle.Core;

   public static class CreatePair
   {
      public static Pair<T, Y> From<T,Y>(T first, Y second)
      {
         return new Pair<T, Y>(first, second);
      }
   }
}
