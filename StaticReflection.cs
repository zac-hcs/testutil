﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;

namespace Hcs.Util
{
   public static class StaticReflection
   {
      /// <summary>
      /// Gets the member info for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      public static MemberInfo GetMemberInfo(Expression<Action> expression)
      {
         if (expression == null)
            throw new ArgumentException("The expression cannot be null.");

         return GetMemberInfo(expression.Body);
      }

      /// <summary>
      /// Gets the member name for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      public static string GetMemberName(Expression<Action> expression)
      {
         return GetMemberInfo(expression).Name;
      }

      /// <summary>
      /// Gets the member info for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      public static MemberInfo GetMemberInfo<T>(Expression<Action<T>> expression)
      {
         if (expression == null)
            throw new ArgumentException("The expression cannot be null.");

         return GetMemberInfo(expression.Body);
      }

      /// <summary>
      /// Gets the member name for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      public static string GetMemberName<T>(Expression<Action<T>> expression)
      {
         return GetMemberInfo(expression).Name;
      }

      /// <summary>
      /// Gets the member info for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      public static MemberInfo GetMemberInfo<T>(Expression<Func<T, object>> expression)
      {
         if (expression == null)
            throw new ArgumentNullException("expression");

         return GetMemberInfo(expression.Body);
      }

      /// <summary>
      /// Gets the member name for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      public static string GetMemberName<T>(Expression<Func<T, object>> expression)
      {
         if (expression == null)
            throw new ArgumentNullException("expression");

         return GetMemberInfo(expression).Name;
      }

      /// <summary>
      /// Gets the member info for the specified expression.
      /// </summary>
      /// <param name="expression">The expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      private static MemberInfo GetMemberInfo(Expression expression)
      {
         if (expression == null)
            throw new ArgumentException("The expression cannot be null.");

         MemberExpression memberExpression = expression as MemberExpression;

         if (memberExpression != null)
         {  // Reference type property or field
            return memberExpression.Member;
         }

         MethodCallExpression methodCallExpression = expression as MethodCallExpression;

         if (methodCallExpression != null)
         {  // Reference type method
            return methodCallExpression.Method;
         }

         UnaryExpression unaryExpression = expression as UnaryExpression;

         if (unaryExpression != null)
         {  // Property, field of method returning value type
            return GetMemberInfo(unaryExpression);
         }

         throw new ArgumentException("Invalid expression");
      }

      /// <summary>
      /// Gets the member info for the specified expression.
      /// </summary>
      /// <param name="unaryExpression">The unary expression.</param>
      /// <returns>The <see cref="MemberInfo"/>.</returns>
      private static MemberInfo GetMemberInfo(UnaryExpression unaryExpression)
      {
         Debug.Assert(unaryExpression != null, "unaryExpression is null");

         MethodCallExpression methodExpression = unaryExpression.Operand as MethodCallExpression;

         if (methodExpression != null)
         {
            return methodExpression.Method;
         }

         return ((MemberExpression)unaryExpression.Operand).Member;
      }
   }
}