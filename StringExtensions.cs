using System;
using System.Linq;

namespace Hcs.Util
{
   public static class StringExtensions
   {
      /// <summary>
      /// Returns the given substring without throwing an ArgumentOutOfRangeException. If the start index or length are out of range then an empty string is returned.
      /// Optionally appends "..." if the value is shortened (The resulting string will still be within the length limit).
      /// </summary>
      public static string SafeSubstring(this string value, int startIndex, int length, bool ellipses = false)
      {
         if (value == null)
            return null;

         if (string.IsNullOrEmpty(value))
            return string.Empty;

         var valueLength = value.Length;

         if (valueLength <= startIndex)
            return string.Empty;

         if (valueLength <= (startIndex + length))
            return value.Substring(startIndex);

         var ellipsesText = "...";
         if (ellipses && length > ellipsesText.Length)
            // Return a substring with ellipses appended. Ensures the resulting string is still within the overall length limit
            return value.Substring(startIndex, length - ellipsesText.Length) + ellipsesText;
         else
            // Return the substring
            return value.Substring(startIndex, length);
      }

      public static string GetValueOrDefaultIfNullOrEmpty(this string input, string defaultInput)
      {
         if (string.IsNullOrEmpty(input))
            return defaultInput;
         return input;
      }

      public static bool Contains(this string source, string toCheck, StringComparison comp)
      {
         return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
      }

      public static string ToLowerFirstChar(this string input)
      {
         var newString = input;
         if (!string.IsNullOrEmpty(newString) && char.IsUpper(newString[0]))
            newString = char.ToLower(newString[0]) + newString.Substring(1);
         return newString;
      }

      public static string TrimEnd(this string inputText, string value, StringComparison comparisonType = StringComparison.CurrentCultureIgnoreCase)
      {
         if (!string.IsNullOrEmpty(value))
         {
            while (!string.IsNullOrEmpty(inputText) && inputText.EndsWith(value, comparisonType))
            {
               inputText = inputText.Substring(0, (inputText.Length - value.Length));
            }
         }

         return inputText;
      }

   }
}