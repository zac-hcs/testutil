namespace Hcs.Util
{
   public class Triple<TFirst, TSecond, TThird>
   {
      public Triple(TFirst first, TSecond second, TThird third)
      {
         First = first;
         Second = second;
         Third = third;
      }

      public TFirst First { get; private set; }
      public TSecond Second { get; private set; }
      public TThird Third { get; private set; }

      public override bool Equals(object obj)
      {
         if (this == obj) return true;
         var triple = obj as Triple<TFirst, TSecond, TThird>;
         if (triple == null)
            return false;
         if (First == null && triple.First != null
             || triple.First == null && First != null
             || !First.Equals(triple.First))
         {
            return false;
         }
         if (Second == null && triple.Second != null
             || triple.Second == null && Second != null
             || !Second.Equals(triple.Second))
         {
            return false;
         }
         if (Third == null && triple.Third != null
             || triple.Third == null && Third != null
             || !Third.Equals(triple.Third))
         {
            return false;
         }

         return true;
      }

      public override int GetHashCode()
      {
         unchecked
         {
            var hash = 17;
            hash = hash*23 + (First != null ? First.GetHashCode() : 0);
            hash = hash*23 + (Second != null ? Second.GetHashCode() : 0);
            hash = hash*23 + (Third != null ? Third.GetHashCode() : 0);
            return hash;
         }
      }
   }
}