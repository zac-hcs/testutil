﻿using System;

namespace Hcs.Util
{
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using System.Linq;
   using System.Reflection;
   using Humanizer;

   public static class EnumHelper
   {
      /// <summary>
      /// Gets an attribute on an enum field value
      /// </summary>
      /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
      /// <param name="enumVal">The enum value</param>
      /// <returns>The attribute of type T that exists on the enum value</returns>
      /// <example>string desc = myEnumVariable.GetAttributeOfType&lt;DescriptionAttribute&gt;().Description;</example>
      public static T GetAttribute<T>(this Enum enumVal) where T : System.Attribute
      {
         // https://stackoverflow.com/questions/1799370/getting-attributes-of-enums-value
         var type = enumVal.GetType();
         var memInfo = type.GetMember(enumVal.ToString());
         var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
         return (attributes.Length > 0) ? (T)attributes[0] : null;
      }

      /// <summary>
      /// Attempts to parse the give value into a valid value for the given enum
      /// </summary>
      /// <param name="type">The enum that the value is to be parsed into</param>
      /// <param name="val">The value to be parsed</param>
      /// <returns>The value parsed into the specified enum type</returns>
      /// <exception cref="ArgumentException"/>
      public static T ParseEnum<T>(string val) where T : struct
      {
         var type = typeof(T);

         if (!type.IsEnum)
            throw new ArgumentException("Cannot parse value to enum: the type specified, '" + type.Name +
                                        "', is not an enum.");
         val = val.Trim();

         // is this a valid value for the specified enum?
         if (Enum.IsDefined(type, val))
         {
            // yes, so lets parse it
            var code = Enum.Parse(type, val);
            if (code is T)
               return (T)code;
         }

         // no, so throw an error
         string validVals = Enum.GetNames(type).Aggregate("", (current, item) => current + (item + " | "));


         throw new ArgumentException("Invalid value for enumeration: '" + val + "' is not in " + type.Name +
                                     ". Valid values are " + validVals + ".");
      }


      public static T? TryParseEnum<T>(string val) where T : struct
      {
         try
         {
            return ParseEnum<T>(val);
         }
         catch (ArgumentException)
         {
            return null;
         }
      }

      public static IEnumerable<TEnumType> GetAllEnum<TEnumType>() where TEnumType : struct
      {
         return Enum.GetValues(typeof(TEnumType)).Cast<TEnumType>();
      }

      /// <summary>
      /// Tries to build a list of the specified enum values from the given string 
      /// </summary>
      /// <typeparam name="TEnumType">The enum type to try to cast the list to</typeparam>
      /// <param name="enumList">The string to build the list from</param>
      /// <returns>The list of parsed enum values</returns>      
      public static List<TEnumType> GetEnumListFromString<TEnumType>(string enumList) where TEnumType : struct
      {
         return GetEnumListFromString<TEnumType>(enumList, ',');
      }

      /// <summary>
      /// Tries to build a list of the specified enum values from the given string 
      /// </summary>
      /// <typeparam name="TEnumType">The enum type to try to cast the list to</typeparam>
      /// <param name="enumList">The string to build the list from</param>
      /// <param name="seperator">The list item seperator</param>
      /// <returns>The list of parsed enum values</returns>
      public static List<TEnumType> GetEnumListFromString<TEnumType>(string enumList, char seperator) where TEnumType : struct
      {
         var l = new List<TEnumType>();

         if (!string.IsNullOrEmpty(enumList))
         {
            var messageList = enumList.Split(',');
            l.AddRange(messageList.Select(m => ParseEnum<TEnumType>(m.Trim().ToUpper())));
         }
         return l;
      }



      /// <summary>
      /// Gets string from "Description" attribute for given enum value. Null if no description attribute.
      /// From http://stackoverflow.com/a/1415187
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static string GetEnumDescription(this Enum value)
      {
         var type = value.GetType();
         var name = Enum.GetName(type, value);
         if (name != null)
         {
            var field = type.GetField(name);
            if (field != null)
            {
               var attr =
                  Attribute.GetCustomAttribute(field,
                     typeof(DescriptionAttribute)) as DescriptionAttribute;
               if (attr != null)
               {
                  return attr.Description;
               }
            }
         }
         return null;
      }

      /// <summary>
      /// Gets Name string from "Display" attribute for given enum value.
      /// From http://stackoverflow.com/a/1415187
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static string GetEnumDisplayName(this Enum value)
      {
         var type = value.GetType();
         var name = Enum.GetName(type, value);
         if (name != null)
         {
            var field = type.GetField(name);
            if (field != null)
            {
               var attr =
                  Attribute.GetCustomAttribute(field,
                     typeof(DisplayAttribute)) as DisplayAttribute;
               if (attr != null)
               {
                  return attr.Name;
               }
            }
         }
         return value.ToString().Humanize(LetterCasing.Title);
      }

      /// <summary>
      /// Creates a List of objects with all labels(keys) and values(descriptions) of a given Enum class
      /// </summary>
      /// <typeparam name="T">Must be derived from class Enum!</typeparam>
      /// <returns>A list of KeyValuePair: string, string; with all available
      /// labels and values of the given Enum.</returns>
      public static List<object> ToList<T>() where T : struct
      {
         var type = typeof(T);

         if (!type.IsEnum)
         {
            throw new ArgumentException("T must be an enum");
         }

         return Enum.GetValues(type)
            .OfType<Enum>()
            .Select(v => new
            {
               value = v.ToString(),
               label = GetEnumDescription(v)
            })
            .ToList<object>();
      }

      /// <summary>
      /// https://stackoverflow.com/questions/5320592/value-is-in-enum-list
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="val"></param>
      /// <param name="values"></param>
      /// <returns></returns>
      public static bool In<T>(this T val, params T[] values) where T : struct
      {
         return values.Contains(val);
      }
   }
}
