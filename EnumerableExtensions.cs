﻿namespace Hcs.Util
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   public static class EnumerableExtensions
   {
      public static ICollection<TType> ToCollection<TType>(this IEnumerable<TType> enumerable)
      {
         var collection = enumerable as ICollection<TType>;
         if (collection != null)
            return collection;
         return enumerable.ToList();
      }

      public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> source, int size)
      {
         T[] bucket = null;
         var count = 0;

         foreach (var item in source)
         {
            if (bucket == null)
               bucket = new T[size];

            bucket[count++] = item;

            if (count != size)
               continue;

            yield return bucket.Select(x => x);

            bucket = null;
            count = 0;
         }

         // Return the last bucket with all remaining elements
         if (bucket != null && count > 0)
            yield return bucket.Take(count);
      }
   }
}
