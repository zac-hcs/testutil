﻿namespace Hcs.Util
{
   using System;
   using System.Text.RegularExpressions;

   public static class ContactDetailsHelper
   {
      // NOTE: these regexes are not 100% perfect - they doesn't ensure matching opening/closing brackets on area code.
      // ie, it's possible to have an opening bracket and no closing bracket (or vice versa) and it'll still match ok.
      public const string RegexAussieLandlineAreaCodeBracketsOptional = @"\(?(?<areaCode>0\d)\)?";
      public const string RegexAussieLandlineLocalNumber = @"(?<phoneNumberStartGroup>\d{4})(?<phoneNumberEndGroup>\d{4})";
      public const string RegexAussieMobileBracketsOptional = @"\(?(?<mobileNumberStartGroup>04\d{2})\)?(?<mobileNumberMiddleGroup>\d{3})(?<mobileNumberEndGroup>\d{3})";

      /// <summary>
      /// Looking for regular Australian landline or mobile numbers, and returning eg:
      /// (03) 6299 9999
      /// 6299 9999 WHERE AREA CODE NOT SPECIFIED
      /// (0499) 999 999
      /// 
      /// Otherwise, will return source value if it cannot parse.
      /// </summary>
      /// <param name="sourceNumber"></param>
      /// <returns></returns>
      public static string GetPhoneNumberTryAustralianFormat(string sourceNumber)
      {
         if (string.IsNullOrEmpty(sourceNumber))
            return sourceNumber;

         try
         {
            //first strip all whitespace
            var noSpaces = Regex.Replace(sourceNumber, @"\s+", "");

            // now check for mobile number (area code brackets optional)
            var mobilePhoneRegex = new Regex(string.Format(@"\A{0}\Z", RegexAussieMobileBracketsOptional));
            var mobileMatch = mobilePhoneRegex.Match(noSpaces);
            if (mobileMatch.Success)
            {
               var mobileNumberStartGroup = mobileMatch.Groups["mobileNumberStartGroup"].Value;
               var mobileNumberMiddleGroup = mobileMatch.Groups["mobileNumberMiddleGroup"].Value;
               var mobileNumberEndGroup = mobileMatch.Groups["mobileNumberEndGroup"].Value;

               // return formatted mobile number
               return string.Format("({0}) {1} {2}", mobileNumberStartGroup, mobileNumberMiddleGroup, mobileNumberEndGroup);
            }

            // wasn't mobile number - check for regular Australian landline number such as "(03) 6299 9999" (area code optional, area code brackets optional)
            var phoneWithAreaCodeRegex = new Regex(string.Format(@"\A({0})?{1}\Z", RegexAussieLandlineAreaCodeBracketsOptional, RegexAussieLandlineLocalNumber));
            var match = phoneWithAreaCodeRegex.Match(noSpaces);

            if (match.Success)
            {
               var areaCode = match.Groups["areaCode"].Value;
               var phoneNumberStartGroup = match.Groups["phoneNumberStartGroup"].Value;
               var phoneNumberEndGroup = match.Groups["phoneNumberEndGroup"].Value;

               return (string.IsNullOrEmpty(areaCode) ? "" : string.Format("({0}) ", areaCode)) +
                  string.Format("{0} {1}", phoneNumberStartGroup, phoneNumberEndGroup);
            }
            // number was not recognised - return source value.
            return sourceNumber;
         }
         catch (Exception)
         {
            // ignore all errors, we'll just return the source if something goes wrong
            return sourceNumber;
         }
      }
   }
}
