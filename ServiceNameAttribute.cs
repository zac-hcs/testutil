﻿using System;
using System.Linq;
using Castle.MicroKernel.Registration;

namespace Hcs.Util
{
   /// <summary>
   /// Gives a name to a service using an Attribute, to allow it to be easily registered with this name in a
   /// dependency injection library.
   /// </summary>
   [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
   public class ServiceNameAttribute : Attribute
   {
      public ServiceNameAttribute(string name)
      {
         Name = name;
      }

      /// <summary>
      /// The name of the service
      /// </summary>
      public string Name { get; private set; }

      /// <summary>
      /// Automatically sets the name of the given service in Windsor Castle (using the ServiceNameAttribute)
      /// </summary>
      /// <param name="c"></param>
      public static void SetServiceNameFromAttribute(ComponentRegistration c)
      {
         var attribute =
            c.Implementation.GetCustomAttributes(typeof(ServiceNameAttribute), false).FirstOrDefault()
            as ServiceNameAttribute;

         if (attribute != null)
         {
            c.Named(attribute.Name);
         }
      }
   }
}
