﻿namespace Hcs.Util.AuthLogging
{
   public enum AuthLogMessageType
   {
      LoginAttempt,
      SignOffAttempt,
      TokenRefresh
   }
}