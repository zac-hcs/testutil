﻿namespace Hcs.Util
{
   using System;
   using System.Globalization;
   using System.Numerics;

   public static class OidHelper
   {
      public static string GuidToOid(Guid guid)
      {
         var uuid = guid.ToString();
         return GuidToOid(uuid);
      }

      public static string GuidToOid(string guid)
      {
         BigInteger value = BigInteger.Parse("00" + guid.Replace("-", ""), NumberStyles.HexNumber);
         return string.Format("2.25.{0}", value);
      }
   }
}
