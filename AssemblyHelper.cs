﻿namespace Hcs.Util
{
   using System.Diagnostics;
   using System.Reflection;
   using System.Web;

   public static class AssemblyHelper
   {
      private const string AspNetNamespace = "ASP";

      private static readonly Assembly _applicationAssembly;
      public static Assembly ApplicationAssembly
      {
         get { return _applicationAssembly; }
      }

      private static readonly FileVersionInfo _fileVersionInfo;
      public static FileVersionInfo FileVersionInfo
      {
         get
         {
            return _fileVersionInfo;
         }
      }

      //cache the values while the application is running
      static AssemblyHelper()
      {
         _applicationAssembly = GetApplicationAssembly();

         if (_applicationAssembly != null)
            _fileVersionInfo = FileVersionInfo.GetVersionInfo(_applicationAssembly.Location);
      }

      //get the root application assembly
      private static Assembly GetApplicationAssembly()
      {
         // Try the EntryAssembly, this doesn't work for ASP.NET classic pipeline (untested on integrated)
         var ass = Assembly.GetEntryAssembly();

         if (ass == null)
         {
            // Look for web application assembly
            var ctx = HttpContext.Current;
            if (ctx != null)
               ass = GetWebApplicationAssembly(ctx);
         }

         // Fallback to executing assembly
         return ass ?? (Assembly.GetExecutingAssembly());
      }
 

      private static Assembly GetWebApplicationAssembly(HttpContext context)
      {
         if (context == null) return null;

         var handler = context.CurrentHandler;
         if (handler == null)
            return null;

         var type = handler.GetType();
         while (type != null && type != typeof(object) && type.Namespace == AspNetNamespace)
            type = type.BaseType;

         if (type == null)
            return null;

         return type.Assembly;
      }
   }
}
